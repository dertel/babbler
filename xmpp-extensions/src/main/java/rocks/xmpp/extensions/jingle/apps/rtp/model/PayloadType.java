package rocks.xmpp.extensions.jingle.apps.rtp.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

/**
 * The payload type which specifies an encoding that can be used for the RTP stream.
 */
public final class PayloadType {

    private List<Parameter> parameter = new ArrayList<>();

    @XmlElement(name = "rtcp-fb", namespace = "urn:xmpp:jingle:apps:rtp:rtcp-fb:0")
    private List<RtcpFeedBack> rtcpFeedback = new ArrayList<>();

    @XmlElement(name = "rtcp-fb-trr-int", namespace = "urn:xmpp:jingle:apps:rtp:rtcp-fb:0")
    private List<RtcpFeedBackInterval> rtcpFeedbackInterval = new ArrayList<>();

    @XmlAttribute
    private int channels = 1;

    @XmlAttribute
    private long clockrate;

    @XmlAttribute
    private int id;

    @XmlAttribute
    private long maxptime;

    @XmlAttribute
    private String name;

    @XmlAttribute
    private long ptime;

    private PayloadType() {
    }

    /**
     * Creates a payload type. The id is the only required attribute.
     *
     * @param id The id.
     */
    public PayloadType(int id) {
        this.id = id;
    }

    /**
     * Creates a payload type with id, name and clockrate
     * @param id        The id
     * @param clockRate The sampling frequency in Hertz
     * @param name      The name
     */
    public PayloadType(int id, long clockRate, String name) {
        this.id = id;
        this.clockrate = clockRate;
        this.name = name;
    }

    /**
     * Creates a payload type with all possible attributes.
     *
     * @param id            The id.
     * @param channels      The number of channels.
     * @param clockRate     The sampling frequency in Hertz.
     * @param name          The name.
     * @param packetTime    The packet time.
     * @param maxPacketTime The maximum packet time.
     */
    public PayloadType(int id, int channels, long clockRate, String name, long packetTime, long maxPacketTime) {
        this.id = id;
        this.channels = channels;
        this.clockrate = clockRate;
        this.ptime = packetTime;
        this.maxptime = maxPacketTime;
        this.name = name;
    }

    /**
     * Set the number of channels
     * @param channels
     */
    public void setChannels(int channels) {
        this.channels = channels;
    }

    /**
     * Sets the sampling frequency in Hertz.
     * @param clockrate
     */
    public void setClockrate(long clockrate) {
        this.clockrate = clockrate;
    }

    /**
     * Sets the maximum packet time as specified in RFC 4566
     * @param maxptime
     */
    public void setMaxptime(long maxptime) {
        this.maxptime = maxptime;
    }

    /**
     * Sets the appropriate subtype of the MIME type.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the packet time as specified in RFC 4566.
     * @param ptime
     */
    public void setPtime(long ptime) {
        this.ptime = ptime;
    }

    /**
     * Gets the appropriate subtype of the MIME type.
     *
     * @return The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the sampling frequency in Hertz.
     *
     * @return The sampling frequency.
     */
    public long getClockRate() {
        return clockrate;
    }

    /**
     * Gets the number of channels.
     *
     * @return The number of channels.
     */
    public int getChannels() {
        return channels;
    }

    /**
     * Gets the parameters. For example, as described in RFC 5574, the "cng", "mode", and "vbr" parameters can be specified in relation to usage of the Speex codec.
     *
     * @return The parameters.
     */
    public List<Parameter> getParameters() {
        return parameter;
    }

    /**
     * Gets the payload identifier.
     *
     * @return The payload identifier.
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the maximum packet time as specified in RFC 4566
     *
     * @return The maximum packet time.
     */
    public long getMaxPacketTime() {
        return maxptime;
    }

    /**
     * Gets the packet time as specified in RFC 4566.
     *
     * @return The packet time.
     */
    public long getPacketTime() {
        return ptime;
    }

    /**
     * Get the list of RTCP-Feedback XEP-0293
     * @return
     */
    public List<RtcpFeedBack> getRtcpFeedback() {
        return rtcpFeedback;
    }

    /**
     * Get the list of RTCP-Feedback-Interval XEP-0293
     * @return
     */
    public List<RtcpFeedBackInterval> getRtcpFeedbackInterval() {
        return rtcpFeedbackInterval;
    }

}

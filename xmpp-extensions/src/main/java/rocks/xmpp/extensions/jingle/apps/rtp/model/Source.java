package rocks.xmpp.extensions.jingle.apps.rtp.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

/**
 * XEP-0339 Source-Specific Media Attributes in Jingle
 */
public class Source {

    public static final String NAMESPACE = "urn:xmpp:jingle:apps:rtp:ssma:0";

    @XmlAttribute
    private String ssrc;

    @XmlElement(name = "parameter")
    private final List<Parameter> parameters = new ArrayList<>();

    public Source() {
    }

    public String getSsrc() {
        return ssrc;
    }

    /**
     * Create a new source element
     * @param ssrc
     */
    public Source(String ssrc) {
        this.ssrc = ssrc;
    }

    /**
     * Get the list of parameter elements for this source
     * @return
     */
    public List<Parameter> getParameters() {
        return parameters;
    }

}

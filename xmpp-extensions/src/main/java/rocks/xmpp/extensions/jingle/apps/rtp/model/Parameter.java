package rocks.xmpp.extensions.jingle.apps.rtp.model;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * The parameter for a payload.
 */
public final class Parameter {

    @XmlAttribute
    private String name;

    @XmlAttribute
    private String value;

    private Parameter() {
    }

    /**
     * Constructs a parameter with name and value.
     *
     * @param name  The name.
     * @param value The value.
     */
    public Parameter(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Gets the parameter name.
     *
     * @return The parameter name.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the parameter value.
     *
     * @return The parameter value.
     */
    public String getValue() {
        return value;
    }
}

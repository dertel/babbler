package rocks.xmpp.extensions.jingle.apps.rtp.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

/**
 * XEP-0339 Source-Specific Media Attributes in Jingle
 */
public class SsrcGroup {

    public static final String NAMESPACE = "urn:xmpp:jingle:apps:rtp:ssma:0";

    public SsrcGroup() {
    }

    @XmlAttribute
    private String semantics;

    @XmlElement(name = "source")
    private final List<Source> sources = new ArrayList<>();

    public List<Source> getSources() {
        return sources;
    }

    public String getSemantics() {
        return semantics;
    }
}

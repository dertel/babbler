/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Christian Schudt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package rocks.xmpp.extensions.jingle.apps.rtp.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlEnumValue;

/**
 * Jingle RTP Header Extension as specified in XEP-0294
 */
public final class RtpHeaderExt {

    public static final String NAMESPACE = "urn:xmpp:jingle:apps:rtp:rtp-hdrext:0";

    @XmlAttribute
    private int id;

    @XmlAttribute
    private String uri;

    @XmlAttribute
    private Senders senders;

    public RtpHeaderExt() {
    }

    /**
     * Create a new RTP Header extension element given an id and URI
     * @param id
     * @param uri
     */
    public RtpHeaderExt(int id, String uri) {
        this.id = id;
        this.uri = uri;
    }

    /**
     * Create a new RTP Header extension element given an id, URI and Senders value
     * @param id
     * @param uri
     * @param senders
     */
    public RtpHeaderExt(int id, String uri, Senders senders) {
        this.id = id;
        this.uri = uri;
        this.senders = senders;
    }

    /**
     * Get the id attribute value
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Get the URI attribute value
     * @return
     */
    public String getUri() {
        return uri;
    }

    /**
     * Get the senders attribute value
     * @return
     */
    public Senders getSenders() {
        return senders;
    }

    public void setSenders(Senders senders) {
        this.senders = senders;
    }

    public enum Senders {
        @XmlEnumValue("both")
        BOTH,
        @XmlEnumValue("initiator")
        INITIATOR,
        @XmlEnumValue("responder")
        RESPONDER
    }
}

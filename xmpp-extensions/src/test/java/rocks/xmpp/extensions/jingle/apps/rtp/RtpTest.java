/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2015 Christian Schudt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package rocks.xmpp.extensions.jingle.apps.rtp;

import org.testng.Assert;
import org.testng.annotations.Test;
import rocks.xmpp.core.XmlTest;
import rocks.xmpp.extensions.jingle.apps.rtp.model.*;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

/**
 * @author Christian Schudt
 */
public class RtpTest extends XmlTest {
    protected RtpTest() throws JAXBException, XMLStreamException {
        super(Rtp.class, PayloadType.class, Parameter.class, Source.class, SsrcGroup.class, RtpHeaderExt.class,
                RtcpFeedBack.class, RtcpFeedBackInterval.class);
    }

    @Test
    public void unmarshalRtp() throws XMLStreamException, JAXBException {
        String xml = "<description xmlns='urn:xmpp:jingle:apps:rtp:1' media='audio'>\n" +
                "        <payload-type id='96' name='speex' clockrate='16000'/>\n" +
                "        <payload-type id='97' name='speex' clockrate='8000'/>\n" +
                "        <payload-type id='18' name='G729'/>\n" +
                "        <payload-type id='0' name='PCMU'/>\n" +
                "        <payload-type id='103' name='L16' clockrate='16000' channels='2'/>\n" +
                "        <payload-type id='98' name='x-ISAC' clockrate='8000'/>\n" +
                "        <payload-type id='96' name='speex' clockrate='16000' ptime='40'>\n" +
                "           <parameter name='vbr' value='on'/>\n" +
                "           <parameter name='cng' value='on'/>\n" +
                "        </payload-type>\n" +
                "       <rtp-hdrext xmlns='urn:xmpp:jingle:apps:rtp:rtp-hdrext:0' id='1' uri='urn:ietf:params:rtp-hdrext:ssrc-audio-level' senders='both' />" +
                "       <rtp-hdrext xmlns='urn:xmpp:jingle:apps:rtp:rtp-hdrext:0' id='3' uri='http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time' senders='both' />" +
                "       <source xmlns='urn:xmpp:jingle:apps:rtp:ssma:0' ssrc='1884242868'>" +
                "           <parameter name='cname' value='5aUSZkHnEbNrdNO6' />" +
                "           <parameter name='msid' value='H2DiF6mxBuAVKWCUurjvtgiLrykeDO10fMUl f155c76a-da98-485a-99ec-44f5b0921655' />" +
                "       </source>" +
                "       <source xmlns=\"urn:xmpp:jingle:apps:rtp:ssma:0\" ssrc=\"788459527\">\n" +
                "           <parameter name=\"cname\" value=\"cHEYRPFR2t2O1HRm\" />\n" +
                "           <parameter name=\"msid\" value=\"Jhzzea7g534Y9hr0NMfDcRE5Qcp6oEpyTykS db882745-d1a8-4a65-9f96-e04660460fb4\" />\n" +
                "       </source>" +
                "       <ssrc-group xmlns='urn:xmpp:jingle:apps:rtp:ssma:0' semantics='FID'>\n" +
                "           <source ssrc='1884242868'/>\n" +
                "           <source ssrc='788459527'/>\n" +
                "       </ssrc-group>" +
                "      </description>\n";

        Rtp rtp = unmarshal(xml, Rtp.class);
        PayloadType payloadType = unmarshal(xml, PayloadType.class);
        Assert.assertEquals(rtp.getMedia(), "audio");
        Assert.assertEquals(rtp.getPayloadTypes().size(), 7);
        Assert.assertEquals(rtp.getPayloadTypes().get(0).getName(), "speex");
        Assert.assertEquals(rtp.getPayloadTypes().get(0).getClockRate(), 16000);
        Assert.assertEquals(rtp.getPayloadTypes().get(4).getChannels(), 2);
        Assert.assertEquals(rtp.getPayloadTypes().get(6).getParameters().size(), 2);
        Assert.assertEquals(rtp.getHeaderExts().size(), 2);

        // Sources
        Assert.assertEquals(rtp.getSources().size(), 2);
        Assert.assertEquals(rtp.getSources().get(0).getSsrc(), "1884242868");
        Assert.assertEquals(rtp.getSources().get(0).getParameters().size(), 2);
        Assert.assertEquals(rtp.getSources().get(0).getParameters().get(0).getName(), "cname");
        Assert.assertEquals(rtp.getSources().get(0).getParameters().get(0).getValue(), "5aUSZkHnEbNrdNO6");
        Assert.assertEquals(rtp.getSources().get(0).getParameters().get(1).getName(), "msid");
        Assert.assertEquals(rtp.getSources().get(0).getParameters().get(1).getValue(), "H2DiF6mxBuAVKWCUurjvtgiLrykeDO10fMUl f155c76a-da98-485a-99ec-44f5b0921655");

        Assert.assertEquals(rtp.getSources().get(1).getSsrc(), "788459527");
        Assert.assertEquals(rtp.getSources().get(1).getParameters().size(), 2);
        Assert.assertEquals(rtp.getSources().get(1).getParameters().get(0).getName(), "cname");
        Assert.assertEquals(rtp.getSources().get(1).getParameters().get(0).getValue(), "cHEYRPFR2t2O1HRm");
        Assert.assertEquals(rtp.getSources().get(1).getParameters().get(1).getName(), "msid");
        Assert.assertEquals(rtp.getSources().get(1).getParameters().get(1).getValue(), "Jhzzea7g534Y9hr0NMfDcRE5Qcp6oEpyTykS db882745-d1a8-4a65-9f96-e04660460fb4");

        // header extensions
        Assert.assertEquals(rtp.getHeaderExts().size(), 2);
        Assert.assertEquals(rtp.getHeaderExts().get(0).getId(), 1);
        Assert.assertEquals(rtp.getHeaderExts().get(0).getUri(), "urn:ietf:params:rtp-hdrext:ssrc-audio-level");
        Assert.assertEquals(rtp.getHeaderExts().get(0).getSenders(), RtpHeaderExt.Senders.BOTH);

        // source group
        Assert.assertEquals(rtp.getSourceGroups().size(), 1);
        Assert.assertEquals(rtp.getSourceGroups().get(0).getSemantics(), "FID");
        Assert.assertEquals(rtp.getSourceGroups().get(0).getSources().size(), 2);
        Assert.assertEquals(rtp.getSourceGroups().get(0).getSources().get(0).getSsrc(), "1884242868");
        Assert.assertEquals(rtp.getSourceGroups().get(0).getSources().get(1).getSsrc(), "788459527");

        Assert.assertEquals(rtp.getPayloadTypes().get(6).getParameters().get(0).getName(), "vbr");
        Assert.assertEquals(rtp.getPayloadTypes().get(6).getParameters().get(0).getValue(), "on");
        Assert.assertEquals(rtp.getPayloadTypes().get(6).getMaxPacketTime(), 0);
    }

    @Test
    public void unmarshalRtpWithCrypto() throws XMLStreamException, JAXBException {
        String xml = "<description xmlns='urn:xmpp:jingle:apps:rtp:1' media='audio'>\n" +
                "        <encryption required='1'>\n" +
                "           <crypto \n" +
                "           crypto-suite='AES_CM_128_HMAC_SHA1_80' \n" +
                "           key-params='inline:WVNfX19zZW1jdGwgKCkgewkyMjA7fQp9CnVubGVz|2^20|1:32' \n" +
                "           session-params='KDR=1 UNENCRYPTED_SRTCP'\n" +
                "           tag='1'/>\n" +
                "       </encryption>\n" +
                "      </description>\n";

        Rtp rtp = unmarshal(xml, Rtp.class);
        Assert.assertNotNull(rtp.getEncryption());
        Assert.assertEquals(rtp.getEncryption().getCrypto().size(), 1);
        Assert.assertEquals(rtp.getEncryption().getCrypto().get(0).getCryptoSuite(), "AES_CM_128_HMAC_SHA1_80");
        Assert.assertEquals(rtp.getEncryption().getCrypto().get(0).getKeyParameters(), "inline:WVNfX19zZW1jdGwgKCkgewkyMjA7fQp9CnVubGVz|2^20|1:32");
        Assert.assertEquals(rtp.getEncryption().getCrypto().get(0).getSessionParameters(), "KDR=1 UNENCRYPTED_SRTCP");
        Assert.assertEquals(rtp.getEncryption().getCrypto().get(0).getTag(), "1");
    }
}
